#!/bin/bash
SERVER_PATH=/home/mariusz/www/express-movies

cp -r dist-server $SERVER_PATH
cp -r public $SERVER_PATH
cp -r data $SERVER_PATH
cp package*.json $SERVER_PATH
cp ecosystem.config.js $SERVER_PATH

(cd $SERVER_PATH && npm install)
