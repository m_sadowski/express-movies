FROM node:12
# Or whatever Node version/image you want
WORKDIR /var/www/app

#Copy the dependencies file
COPY ./package.json ./

#Install dependencies
RUN npm install 

#Copy remaining files
COPY ./ ./

#Install dependencies
RUN npm run build

EXPOSE 3000

#Default command
CMD ["npm","run","server"]