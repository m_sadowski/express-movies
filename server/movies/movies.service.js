import { combineArray } from '../utils/utils';
import {
  getMoviesWithCombinedGenres,
  getRandomMovie,
} from './movies.utils';
import NotFoundError from '../utils/errors/notFoundError';

class MoviesService {
  constructor({ moviesRepository }) {
    this.moviesRepository = moviesRepository;
  }

  async getMovies({ duration = '0', genres = '' }) {
    // eslint-disable-next-line radix
    const durationNum = parseInt(duration) || 0;
    const durationRange = durationNum > 0 ? [durationNum - 10, durationNum + 10] : [];
    const result = [];
    const movies = await this.moviesRepository.getMoviesAll();

    if (genres.length > 0) {
      const combinedFilterGenres = combineArray(genres.split(','), 1).reverse();
      result.push(
        ...getMoviesWithCombinedGenres(movies, combinedFilterGenres, durationRange),
      );
    } else {
      const randomMovie = getRandomMovie(movies, durationRange);
      if (randomMovie) {
        result.push(randomMovie);
      }
    }

    if (result.length === 0) {
      throw new NotFoundError('Movies not found');
    }

    return result;
  }

  async addMovie(movie) {
    return this.moviesRepository.addMovieQueued(movie);
  }
}

export default MoviesService;
