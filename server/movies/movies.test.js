import path from 'path';
import MoviesFileProvider from './movies.file.provider';
import MoviesInMemoryProvider from './movies.inmemory.provider';
import MoviesRepository from './movies.repository';
import MoviesService from './movies.service';
import { saveMovieMiddleware, validateMovieMiddleware } from './movies.controller';
import { verifyGenres } from './movies.utils';

describe('Movies tests', () => {
  describe('Get', () => {
    let moviesService;

    beforeAll(() => {
      const moviesProvider = new MoviesFileProvider(
        path.join(__dirname, '../../data/db.test.json'),
      );
      const moviesRepository = new MoviesRepository({ moviesProvider, validateData: true });
      moviesService = new MoviesService({ moviesRepository });
    });

    test('random movie (without duration & genres provided)', () => moviesService.getMovies({}).then((movies) => {
      expect(movies).toHaveLength(1);
    }));

    test('random movie (with duration provided)', async () => {
      const duration = 120;
      const durationRange = [duration - 10, duration + 10];
      const movies = await moviesService.getMovies({ duration: duration.toString() });

      expect(movies).toHaveLength(1);
      // eslint-disable-next-line radix
      expect(parseInt(movies[0].runtime)).toBeGreaterThanOrEqual(
        durationRange[0],
      );
      // eslint-disable-next-line radix
      expect(parseInt(movies[0].runtime)).toBeLessThanOrEqual(
        durationRange[1],
      );
    });

    test('movies (with genres provided)', async () => {
      const genres = 'Adventure,Drama,Romance';
      const genresArr = genres.split(',');
      const movies = await moviesService.getMovies({ genres });

      expect(movies.length).toBeGreaterThan(0);
      expect(movies[0].id).toBe(18);
      expect(
        verifyGenres(movies, genresArr),
      ).toBe(true);
    });

    test('movies (with genres and duration provided)', async () => {
      const genres = 'Drama,Biography,Music';
      const genresArr = genres.split(',');
      const duration = 110;
      const durationRange = [duration - 10, duration + 10];
      const movies = await moviesService.getMovies({ genres, duration: duration.toString() });

      expect(movies.length).toBeGreaterThan(0);
      expect(movies[0].id).toBe(20);
      expect(
        verifyGenres(
          movies,
          genresArr,
          durationRange,
        ),
      ).toBe(true);
    });

    test('movies (with genres and duration provided) - should fail verification (reversed result order)', async () => {
      const genres = 'Thriller,Mystery,Film-Noir';
      const genresArr = genres.split(',');
      const duration = 103;
      const durationRange = [duration - 10, duration + 10];
      const movies = await moviesService.getMovies({ genres, duration: duration.toString() });

      expect(movies.length).toBeGreaterThan(0);
      expect(movies[0].id).toBe(17);
      movies.reverse(); // reverse movies to check if verifyGenres work properly
      expect(
        verifyGenres(
          movies,
          genresArr,
          durationRange,
        ),
      ).toBe(false);
    });
  });

  describe('Save', () => {
    let moviesService;
    let moviesRepository;

    beforeEach(() => {
      const data = {
        genres: ['Comedy', 'Animation', 'Family'],
        movies: [
          {
            id: 1,
            title: 'Beetlejuice',
            year: '1988',
            runtime: '92',
            genres: ['Comedy', 'Fantasy'],
            director: 'Tim Burton',
          },
        ],
      };
      const moviesProvider = new MoviesInMemoryProvider(data);
      moviesRepository = new MoviesRepository({ moviesProvider });
      moviesService = new MoviesService({ moviesRepository });
    });

    test('add valid movie', async () => {
      const newMovie = {
        title: 'New Movie 1',
        year: '2020',
        runtime: '136',
        genres: ['Comedy', 'Family'],
        director: 'Tom Lee',
      };
      const movies = await moviesService.getMovies({});
      expect(movies).toHaveLength(1);

      const savedMovie = await moviesService.addMovie(newMovie);
      expect(savedMovie.id).toBe(2);
      expect(savedMovie.title).toBe(newMovie.title);
      expect(savedMovie.year).toBe(newMovie.year);
    });

    test('add valid movie (controller)', async () => {
      const newMovie = {
        title: 'New Movie 1',
        year: '2020',
        runtime: '136',
        genres: ['Comedy', 'Family'],
        director: 'Tom Lee',
      };
      const movies = await moviesService.getMovies({});
      expect(movies).toHaveLength(1);

      const middleware = saveMovieMiddleware(moviesService);
      const res = {
        status: jest.fn(() => res),
        json: jest.fn(),
      };
      const req = { body: newMovie };

      await middleware(req, res);
      expect(res.json.mock.calls.length).toBe(1);
      expect(res.status.mock.calls[0][0]).toBe(201); // check if status is 201

      // eslint-disable-next-line max-len
      const savedMovie = res.json.mock.calls[0][0]; // check if returned object is equal to newMmovie
      expect(savedMovie.id).toBe(2);
      expect(savedMovie.title).toBe(newMovie.title);
      expect(savedMovie.year).toBe(newMovie.year);
    });

    test('add invalid movie (without required properties)', async () => {
      const newMovie = {
        title: 'New Movie 1',
        runtime: '136',
        genres: ['Comedy', 'Family'],
      };
      const movies = await moviesService.getMovies({});
      expect(movies).toHaveLength(1);

      const middleware = validateMovieMiddleware(moviesRepository);
      const nextCallback = jest.fn();
      const req = { body: newMovie };
      await middleware(req, null, nextCallback);

      expect(nextCallback.mock.calls.length).toBe(1);
      const errorObj = nextCallback.mock.calls[0][0]; // get first argument of first call
      expect(errorObj instanceof Error).toBe(true);
      expect(errorObj.message).toMatch('"year" is required');
    });

    test('add invalid movie (wrong genre)', async () => {
      const newMovie = {
        title: 'New Movie 1',
        year: '2020',
        runtime: '136',
        genres: ['Comedy', 'Dramat'],
        director: 'Tom Lee',
      };

      const movies = await moviesService.getMovies({});
      expect(movies).toHaveLength(1);

      const middleware = validateMovieMiddleware(moviesRepository);
      const nextCallback = jest.fn();
      const req = { body: newMovie };
      await middleware(req, null, nextCallback);

      expect(nextCallback.mock.calls.length).toBe(1);
      const errorObj = nextCallback.mock.calls[0][0]; // get first argument of first call
      expect(errorObj instanceof Error).toBe(true);
      expect(errorObj.message).toMatch('"genres[1]" must be one of [Comedy, Animation, Family]');
    });
  });
});
