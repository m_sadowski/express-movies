import { randomInt, combineArray, getJoiValidationMessage } from '../utils/utils';
import { createMovieSchema } from './movies.schema';
import ValidationError from '../utils/errors/validationError';

/**
 * Get id for new movie to save
 * @param {Array} movies
 */
const getNextMovieId = (movies) => Math.max(...movies.map((m) => m.id)) + 1;

/**
 * Check if movie runtime is in provided range.
 * Returns boolean
 * @param {Object} movie movie
 * @param {Array[2]} durationRange [from, to]
 */
const checkMovieDuration = (movie, durationRange) => movie.runtime >= durationRange[0]
  && movie.runtime <= durationRange[1];

/**
 * Check if all items of filterGenres (array of string) exists in movie.genres.
 * Returns boolean
 * @param {Object} movie movie
 * @param {Array of strings} filterGenres array of filter genres
 */
const checkMovieGenres = (movie, filterGenres) => filterGenres.every(
  (g) => movie.genres.includes(g),
);

/**
 * Verify if movies match to combined genres provided. Used in unit tests
 * @param {Array} movies
 * @param {Array} genresArr
 * @param {Array[2]} durationRange
 */
const verifyGenres = (movies, genresArr, durationRange = []) => {
  const combinedGenres = combineArray(genresArr).reverse();
  let moviesPassedCount = 0;
  let combinedGenresIndex = 0;

  movies.forEach((movie) => {
    let movieHasAllGenres = false;
    do {
      const genres = combinedGenres[combinedGenresIndex];
      const genresDiff = genresArr.filter((g) => !genres.includes(g));
      // eslint-disable-next-line max-len
      movieHasAllGenres = (genres.every((g) => movie.genres.includes(g))) && !(movie.genres.some((g) => genresDiff.includes(g)));

      if (movieHasAllGenres) {
        if (
          (durationRange.length === 2
            && movie.runtime >= durationRange[0]
              && movie.runtime <= durationRange[1])
          || durationRange.length === 0
        ) {
          moviesPassedCount += 1;
        }
      } else {
        combinedGenresIndex += 1;
      }
    } while (!movieHasAllGenres && (combinedGenresIndex < combinedGenres.length));
  });
  return moviesPassedCount === movies.length;
};

/**
 * Returns random movie from provided movies array
 * @param {Array} movies
 */
const randomMovie = (movies) => {
  if (movies.length > 0) {
    return movies[randomInt(0, movies.length - 1)];
  }
  return null;
};

/**
 * Filters provided movies by genres and duration range.
 * Returns [result (filtered movies), remaining]
 * @param {Array} movies movies
 * @param {Array} filterGenres array of filter genres, can be empty
 * @param {Array[2]} durationRange duration range [from, to], can be empty
 */
const getMovies = (movies, filterGenres, durationRange) => {
  const result = [];
  const remaining = [];
  for (let i = 0; i < movies.length; i += 1) {
    const movie = movies[i];
    let found = false;
    let genresPassed = true;
    let durationPassed = true;

    if (filterGenres.length > 0 && !checkMovieGenres(movie, filterGenres)) {
      genresPassed = false;
    }
    if (
      durationRange.length === 2
      && !checkMovieDuration(movie, durationRange)
    ) {
      durationPassed = false;
    }
    if (genresPassed && durationPassed) {
      found = true;
      result.push(movie);
    }
    if (!found) {
      remaining.push(movie);
    }
  }
  return [result, remaining];
};

/**
 *
 * @param {Array} movies
 * @param {Array} combinedFilterGenres
 * @param {Array[2]} durationRange
 */
const getMoviesWithCombinedGenres = (
  movies,
  combinedFilterGenres,
  durationRange,
) => {
  const result = [];
  let moviesToFilter = movies;
  combinedFilterGenres.forEach((filterGenres) => {
    const filterResult = getMovies(moviesToFilter, filterGenres, durationRange);
    result.push(...filterResult[0]);
    // eslint-disable-next-line prefer-destructuring
    moviesToFilter = filterResult[1];
  });
  return result;
};

/**
 * Gets random movie, optionally filtered by duration
 * @param {object} movies
 * @param {Array[2]} durationRange
 */
const getRandomMovie = (movies, durationRange) => {
  const [moviesFiltered] = getMovies(movies, [], durationRange);
  const random = randomMovie(moviesFiltered);
  return random;
};

const validateMovie = (movie, genresAllowed) => {
  const schema = createMovieSchema(true, genresAllowed);
  const validationResult = schema.validate(movie);
  if (validationResult.error) {
    throw new ValidationError(`Movie is not valid: ${getJoiValidationMessage(validationResult.error)}`);
  }
  return validationResult.value;
};

export {
  getNextMovieId,
  getMovies,
  getMoviesWithCombinedGenres,
  getRandomMovie,
  verifyGenres,
  validateMovie,
};
