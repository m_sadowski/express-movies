// import AsyncLock from 'async-lock';
import Queue from 'bull';
import { getJoiValidationMessage, sleep } from '../utils/utils';
import { getDebug } from '../utils/debug';
import { createMoviesFileSchema } from './movies.schema';
import { getNextMovieId } from './movies.utils';
import { REDIS_URL, REDIS_PORT } from '../utils/env';

const debug = getDebug();

class MoviesRepository {
  constructor({ moviesProvider, validateData = false, useLock = false }) {
    this.moviesProvider = moviesProvider;
    this.validateData = validateData;
    this.useLock = useLock;

    debug(`use lock: ${this.useLock}`);

    // initialize lock object to safely add new movies
    if (this.useLock) {
      // global.movieLock = new AsyncLock();
      this.addMovieQueue = new Queue('addMovie', {
        redis: {
          host: REDIS_URL,
          port: REDIS_PORT,
          // password: 'root',
        },
      });
      this.addMovieQueueOptions = {
        attempts: 1,
      };
      this.addMovieQueue.process(async (job) => {
        // Do this job after adding new item to queue
        debug('process add movie start');
        const result = await this.addMovie(job.data.movie);
        debug(`process add movie finish, movie id: ${result.id}, title: ${result.title}`);
        return result;
      });
    }
  }

  async getData() {
    const data = await this.moviesProvider.getDB();
    if (this.validateData) {
      return this.validateDataObject(data);
    }
    return data;
  }

  getMoviesAll() {
    return this.getData().then((data) => data.movies);
  }

  getGenresAll() {
    return this.getData().then((data) => data.genres);
  }

  // eslint-disable-next-line class-methods-use-this
  validateDataObject(data) {
    return new Promise((resolve, reject) => {
      const schema = createMoviesFileSchema([]);
      const validationResult = schema.validate(data);
      if (validationResult.error) {
        const error = new Error(`DB is not valid: ${getJoiValidationMessage(validationResult.error)}`);
        return reject(error);
      }
      return resolve(validationResult.value);
    });
  }

  /**
   * Adds movie to provider database.
   * Returns saved movie
   * @param {object} movie
   */
  async addMovie(movie) {
    const data = await this.getData();
    const newMovie = { ...movie, id: getNextMovieId(data.movies) };

    // simulate 10 sec operation
    if (this.useLock) {
      await sleep(10 * 1000);
    }

    data.movies.push(newMovie);
    this.moviesProvider.saveDB(data);

    debug(`add movie, id:${newMovie.id}, title:${newMovie.title}`);

    return newMovie;
  }

  async addMovieQueued(movie) {
    if (this.useLock) {
      const job = await this.addMovieQueue.add({ movie }, this.addMovieQueueOptions);
      // return result.data.movie;
      debug(`job id ${job.id}`);
      const result = await job.finished();
      debug('finished addMovieQueued');
      return result;
    }
    const result = await this.addMovie(movie);
    return result;
  }
}

export default MoviesRepository;
