import Joi from '@hapi/joi';

const createMovieSchema = (adding = true, genresAllowed = []) => {
  let idSchema = Joi.number();
  let genresItemsSchema = Joi.string();

  if (!adding) {
    idSchema = idSchema.required();
  }
  if (genresAllowed.length > 0) {
    genresItemsSchema = genresItemsSchema.valid(...genresAllowed);
  }

  const schema = Joi.object({
    id: idSchema,
    title: Joi.string()
      .max(255)
      .required(),
    year: Joi.number().required(),
    runtime: Joi.number().required(),
    director: Joi.string()
      .max(255)
      .required(),
    actors: Joi.string().allow(''),
    plot: Joi.string().allow(''),
    posterUrl: Joi.string().allow(''),
    genres: Joi.array().items(genresItemsSchema),
  });

  return schema;
};

const createMoviesFileSchema = (genresAllowed) => {
  const movieSchema = createMovieSchema(false, genresAllowed);
  const schema = Joi.object({
    genres: Joi.array().items(Joi.string()).required(),
    movies: Joi.array().items(
      movieSchema,
    ).required(),
  });

  return schema;
};

export {
  createMovieSchema,
  createMoviesFileSchema,
};
