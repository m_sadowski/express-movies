import { validateMovie } from './movies.utils';

export const validateMovieMiddleware = (moviesRepository) => {
  const func = async (req, res, next) => {
    const genresAllowed = await moviesRepository.getGenresAll();
    try {
      validateMovie(req.body, genresAllowed);
    } catch (err) {
      return next(err);
    }
    return next();
  };
  return func;
};

export const saveMovieMiddleware = (moviesService) => {
  const func = async (req, res, next) => {
    try {
      const result = await moviesService.addMovie(req.body);
      return res.status(201).json(result);
    } catch (err) {
      return next(err);
    }
  };
  return func;
};

export const getMoviesMiddleware = (moviesService) => {
  const func = async (req, res, next) => {
    const { duration = '0', genres = '' } = req.query;
    try {
      const movies = await moviesService.getMovies({ duration, genres });
      return res.json(movies);
    } catch (err) {
      return next(err);
    }
  };
  return func;
};
