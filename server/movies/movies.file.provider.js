import fs from 'fs';
import path from 'path';

class MoviesFileProvider {
  constructor(filePath = '') {
    if (filePath) {
      this.fileDBPath = filePath;
    } else {
      this.fileDBPath = path.join(__dirname, '../../data/db.json');
    }
  }

  /**
   * Gets database from file.
   * Returns object.
   */
  getDB() {
    return new Promise((resolve, reject) => {
      fs.readFile(this.fileDBPath, 'utf8', (err, data) => {
        if (err) {
          reject(err);
          return;
        }
        try {
          resolve(JSON.parse(data));
        } catch (error) {
          reject(error);
        }
      });
    });
  }

  /**
   * Saves object to file
   * @param {object} data
   */
  saveDB(data) {
    return new Promise((resolve, reject) => {
      const stringData = JSON.stringify(data, null, 4);
      fs.writeFile(this.fileDBPath, stringData, (err) => {
        if (err) {
          reject(err);
        }
        resolve(true);
      });
    });
  }
}

export default MoviesFileProvider;
