class MoviesInMemoryProvider {
  constructor(initialData = {}) {
    this.data = initialData;
  }

  /**
   * Gets database from memory.
   * Returns object.
   */
  getDB() {
    return new Promise((resolve) => {
      resolve(this.data);
    });
  }

  /**
   * Saves object to memory
   * @param {object} data
   */
  saveDB(data) {
    return new Promise((resolve) => {
      this.data = data;
      resolve(true);
    });
  }
}

export default MoviesInMemoryProvider;
