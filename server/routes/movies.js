import express from 'express';
import { USE_QUEUE } from '../utils/env';
import MoviesFileProvider from '../movies/movies.file.provider';
import MoviesRepository from '../movies/movies.repository';
import MoviesService from '../movies/movies.service';
// import { validateMovie } from '../movies/movies.utils';
import { getMoviesMiddleware, saveMovieMiddleware, validateMovieMiddleware } from '../movies/movies.controller';

const router = express.Router();
const moviesProvider = new MoviesFileProvider();
const moviesRepository = new MoviesRepository({
  moviesProvider,
  useLock: USE_QUEUE,
  validateData: true,
});
const moviesService = new MoviesService({ moviesRepository });

router.get('/', getMoviesMiddleware(moviesService));

router.post('/', validateMovieMiddleware(moviesRepository), saveMovieMiddleware(moviesService));

module.exports = router;
