import Debug from 'debug';

const getDebug = (name = 'express-movies:server') => Debug(name);

// eslint-disable-next-line import/prefer-default-export
export { getDebug };
