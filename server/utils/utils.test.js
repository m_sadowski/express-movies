import * as utils from './utils';

describe('Utils tests', () => {
  test('combine array test', () => {
    const inputArray1 = [1, 2];
    const expectedArray1 = [[1], [2], [1, 2]];
    const resultArray1 = utils.combineArray(inputArray1, 1);
    expect(utils.compareJson(resultArray1, expectedArray1)).toBe(true);

    const inputArray2 = [3, 4, 5];
    const expectedArray2 = [[3], [4], [5], [3, 4], [3, 5], [4, 5], [3, 4, 5]];
    const resultArray2 = utils.combineArray(inputArray2, 1);
    expect(utils.compareJson(resultArray2, expectedArray2)).toBe(true);

    const inputArray3 = [6, 7, 8, 9];
    const expectedArray3 = [
      [6],
      [7],
      [8],
      [9],
      [6, 7],
      [6, 8],
      [6, 9],
      [7, 8],
      [7, 9],
      [8, 9],
      [6, 7, 8],
      [6, 7, 9],
      [6, 8, 9],
      [7, 8, 9],
      [6, 7, 8, 9],
    ];
    const resultArray3 = utils.combineArray(inputArray3, 1);
    expect(utils.compareJson(resultArray3, expectedArray3)).toBe(true);
  });
});
