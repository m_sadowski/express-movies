/**
 * Generate all combinations of array items
 * for example (([1,2,3], 1) => [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3]]).
 * https://stackoverflow.com/questions/5752002/find-all-possible-subset-combos-in-an-array
 * @param {*} arr input array
 * @param {number} minLength min length of combination
 */
const combineArray = (arr = [], minLength = 1) => {
  const combine = (a, min) => {
    const fn = (n, src, got, all) => {
      if (n === 0) {
        if (got.length > 0) {
          all[all.length] = got;
        }
        return;
      }
      for (let j = 0; j < src.length; j += 1) {
        fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
      }
      return;
    };
    const all = [];
    for (let i = min; i < a.length; i += 1) {
      fn(i, a, [], all);
    }
    all.push(a);
    return all;
  };
  return combine(arr, minLength);
};

const compareJson = (object1, object2) => JSON.stringify(object1) === JSON.stringify(object2);

const randomInt = (min, max) => min + Math.floor((max - min) * Math.random());

const getJoiValidationMessage = (error) => {
  let message = 'Validation error';
  try {
    message = error.details[0].message;
  // eslint-disable-next-line no-empty
  } catch (err) {}
  return message;
};

const sleep = (milliseconds) => new Promise((resolve) => setTimeout(resolve, milliseconds));

export {
  combineArray, compareJson, getJoiValidationMessage, randomInt, sleep,
};
