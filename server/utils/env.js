const {
  NODE_ENV, PORT, REDIS_URL, REDIS_PORT, REDIS_USER, REDIS_PASS,
} = process.env;

const USE_QUEUE = process.env.USE_QUEUE === '1';

export {
  NODE_ENV,
  PORT,
  USE_QUEUE,
  REDIS_URL,
  REDIS_PORT,
  REDIS_USER,
  REDIS_PASS,
};
