import StatusError from './statusError';

class NotFoundError extends StatusError {
  constructor(message) {
    super(message, 404);
  }
}

export default NotFoundError;
