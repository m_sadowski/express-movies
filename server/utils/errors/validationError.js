import StatusError from './statusError';

class ValidationError extends StatusError {
  constructor(message) {
    super(message, 400);
  }
}

export default ValidationError;
