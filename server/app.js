import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import moviesRoute from './routes/movies';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));
app.use('/movies', moviesRoute);
app.use((err, req, res, next) => {
  // handle errors

  if (process.env.NODE_ENV === 'development') {
    return next(err);
  }
  // production
  res.status(err.status || 500);
  return res.json({ error: err.message });
});

export default app;
