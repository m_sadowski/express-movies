// PM2 ecosystem file

module.exports = {
  apps: [{
    name: 'express-movies-cluster',
    script: 'dist-server/bin/www.js',
    env: {
      NODE_ENV: 'development',
      DEBUG: 'express-movies:server',
    },
    instances: 2,
    exec_mode: 'cluster',
  }],
};
