
## Movies app

Not so long ago we decided create a catalogue of our favourite movies (data/db.json) as json. It is hard to update, so we would like to build an API
for it, however we don't need a database, we still need it as a file.

1. We need to be able to add a new movie. Each movie should contain information about:

- a list of genres (only predefined ones from db file) (required, array of predefined strings)
- title (required, string, max 255 characters)
- year (required, number)
- runtime (required, number)
- director (required, string, max 255 characters)
- actors (optional, string)
- plot (optional, string)
- posterUrl (optional, string)

Each field should be properly validated and meaningful error message should be return in case of invalid value.

2. We also need an endpoint to return a random matching movie for us. What we want to do is to send a list of genres (this parameter is optional) and a duration of a movie we are looking for.

The special algorythm should first find all the movies that have all genres of our choice and runtime between <duration - 10> and <duration + 10>. Then it should repeat this algorytm for each genres combination. For example:

If we send a request with genres [Comedy, Fantasy, Crime] then the top hits should be movies that have all three of them, then there should be movies that have one of [Comedy, Fantasy][comedy, criem], [Fantasy, Crime] and then those with Comedy only, Fantasy only and Crime only.

Of course we dont want to have duplicates.

If we dont provide genres parameter then we get a single random movie with a runtime between <duration - 10> and <duration + 10>.

If we dont provide duration parameter then we should get all of the movie with specific genres.

If we dont provide any parameter, then we should get a single random movie.


### Installation

- npm install

- copy data/db.pattern.json to data/db.json (*cp data/db.pattern.json data/db.json*)

### Running

By default, application uses /data/db.json file as database. Please, ensure if file exists.

  

Commands:

  

-  `npm run dev` app with dev config

-  `npm run prod` app with production config

-  `npm run watch:dev` development server with nodemon

-  `npm run test` jest unit tests

  

Application by default runs on port 3000. You can change it by setting PORT environment variable.

### Endpoints and parameters

Get movies with duration <90,110> and provided genres. On top of results there will be movies with all matched genres [Adventure,Fantasy,Family], then with 2 matched genres, then with 1 matched genre.  

    GET /movies?duration=100&genres=Adventure,Fantasy,Family

Get movies with provided genres. 

    GET /movies?genres=Adventure,Action,Sci-Fi

Get random movie with duration <120,140>

    GET /movies?duration=130

Get random movie

    GET /movies

Add movie 

    POST /movies

Sample POST body to add new movie:

    {
		"title": "Test Movie 1",
        "year": "2020",
        "runtime": "155",
        "genres": [
	        "Drama",
		    "Thriller",
    		"Comedy"
        ],
        "director": "Fake director",
        "actors": "Fake actors",
        "plot": "Plot description",
        "posterUrl": "Poster url"
    }

### Author

Mariusz Sadowski